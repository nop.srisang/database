package database

import (
	"gopkg.in/mgo.v2"
	"time"
)

// MongoDB type
type MongoDB struct {
	DBSession *mgo.Session
	DBInfo    *DBInfo
}

// DBInfo type
type DBInfo struct {
	Host     string
	Database string
	Username string
	Password string
}

// MongoConnect to make database connection
func MongoConnect(dbInfo *DBInfo) (*MongoDB, error) {
	dbDial := &mgo.DialInfo{
		Addrs:    []string{dbInfo.Host},
		Timeout:  60 * time.Second,
		Database: dbInfo.Database,
		Username: dbInfo.Username,
		Password: dbInfo.Password,
	}

	session, err := mgo.DialWithInfo(dbDial)
	if err != nil {
		return nil, err
	}
	mongo := &MongoDB{
		DBSession: session,
		DBInfo:    dbInfo,
	}
	return mongo, err
}
