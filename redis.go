package database

import (
	"github.com/go-redis/redis"
	"time"
)

// RedisConnection type
type RedisConnection struct {
	Conn *redis.Client
}

// RedisInfo type
type RedisInfo struct {
	Addr     string
	Password string
	DB       int
}

// RedisConnect connect to redis
func RedisConnect(redisInfo *RedisInfo) (*RedisConnection, error) {
	conn := redis.NewClient(&redis.Options{
		Addr:        redisInfo.Addr,
		Password:    redisInfo.Password,
		DB:          redisInfo.DB,
		DialTimeout: 3 * time.Second,
	})
	_, err := conn.Ping().Result()
	connection := &RedisConnection{
		Conn: conn,
	}
	return connection, err
}
